--Open CMD Terminal, 

mysql -u root

--type the following,
CREATE DATABASE blog_db;

--change to blog_db,
USE blog_db;


-- Create a Users Table,

CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(320) NOT NULL,
    password VARCHAR(320) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id)
);

-- Create a Posts Table,

CREATE TABLE posts (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    title VARCHAR(200) NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_posts_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

-- Create a Posts_likes,

CREATE TABLE posts_likes (
	id INT AUTO_INCREMENT NOT NULL,
	datetime_liked DATETIME DEFAULT CURRENT_TIMESTAMP,
    post_id INT,
    user_id INT,
    PRIMARY KEY(id),
		FOREIGN KEY(post_id) 
		REFERENCES posts(id)
			ON UPDATE CASCADE
	    	ON DELETE CASCADE,
		FOREIGN KEY(user_id) 
		REFERENCES posts(id)
			ON UPDATE CASCADE
	    	ON DELETE CASCADE
);

-- Create a Posts_comments,

CREATE TABLE posts_comments (
	id INT AUTO_INCREMENT NOT NULL,
	content VARCHAR(255) NOT NULL,
	datetime_commented DATETIME DEFAULT CURRENT_TIMESTAMP,
    post_id INT,
    user_id INT,
    PRIMARY KEY(id),
		FOREIGN KEY(post_id) 
		REFERENCES posts(id)
			ON UPDATE CASCADE
	    	ON DELETE CASCADE,
		FOREIGN KEY(user_id) 
		REFERENCES posts(id)
			ON UPDATE CASCADE
	    	ON DELETE CASCADE
);


